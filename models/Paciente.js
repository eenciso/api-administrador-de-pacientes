// Importando Mongoose
const mongoose = require('mongoose');
// Importando el Schema
const Schema = mongoose.Schema;
// Creando el Schema
const pacientesSchema = new Schema({
	nombre: {
		type: String,
		trim: true
	},
	propietario: {
		type: String,
		trim: true
	},
	fecha: {
		type: String,
		trim: true
	},
	hora: {
		type: String,
		trim: true
	},
	sintomas: {
		type: String,
		trim: true
	}
});

// Haciendo disponible el modelo para importar
module.exports= mongoose.model('Paciente', pacientesSchema);