// Importando Express
const express = require('express');
// Importando Mongoose
const mongoose = require('mongoose');
// Importando el Routing (archivo de rutas)
const routes = require('./routes');
// Importando bodyParser
const bodyParser = require('body-parser');

// Creando el Servidor
const app = express();

// Conectar a mongodb
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/veterinaria', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

// Habilitando bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extend: true}));

// Habilitando Routing
app.use('/', routes());

// Definiendo el puerto y arrancando el servidor
app.listen(4000, () => {
	console.log('Servidor Funcionando!');
})

