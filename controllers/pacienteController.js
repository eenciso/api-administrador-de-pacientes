// Importando el Modelo
const Paciente = require('../models/Paciente.js');

// Creando un nuevo cliente
exports.nuevoCliente = async (request, response, next) => {
	// Crear objeto con datos del paciente
	const paciente = new Paciente(request.body);
	// Insertar en la base de datos
	try {
		await paciente.save();
		// Respuesta hacia la api
		response.json({ mensaje: 'El cliente ha sido agregado correctamente.' });
	} catch(e) {
		// statements
		console.log(e);
		next();
	}
}

// Obtiene todos los pacientes
exports.obtenerPacientes = async (request, response, next) => {
	try {
		const pacientes = await Paciente.find({});
		response.json(pacientes);
	} catch(e) {
		// statements
		console.log(e);
		next();
	}
}

// Obtiene un paciente por su ID
exports.obtenerPaciente = async (request, response, next) => {
	try {
		const paciente = await Paciente.findById(request.params.id);
		response.json(paciente)
	} catch(e) {
		// statements
		console.log(e);
		next();
	}
}

// Actualizar paciente por ID
exports.actualizarPaciente = async (request, response, next) => {
	try {
		const paciente = await Paciente.findOneAndUpdate({_id : request.params.id}, request.body, { new: true});
		response.json(paciente);
	} catch(e) {
		// statements
		console.log(e);
		next();
	}
}

// Elimina paciente por su ID
exports.eliminarPaciente = async (request, response, next) => {
	try {
		await Paciente.findOneAndDelete({_id : request.params.id});
		response.json({mensaje: 'El paciente fue Eliminado'});
	} catch(e) {
		// statements
		console.log(e);
		next();
	}
}