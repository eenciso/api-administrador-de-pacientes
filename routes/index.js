// Importando Express
const express = require('express');
// Variable para alojar los metodos de routing
const router = express.Router();
// Importando el Controlador
const pacienteController = require('../controllers/pacienteController.js');

module.exports = function () {
	// Agrega nuevos Pacientes via POST (ruta , controlador)
	router.post('/pacientes', pacienteController.nuevoCliente);
	// Obtiene todos los registros de pacientes en la BD
	router.get('/pacientes', pacienteController.obtenerPacientes);
	// Obtiene un paciente por su ID
	router.get('/pacientes/:id', pacienteController.obtenerPaciente);
	// Actualizar registro con ID especifico
	router.put('/pacientes/:id', pacienteController.actualizarPaciente);
	// Elimina un registro
	router.delete('/pacientes/:id', pacienteController.eliminarPaciente);

	return router;
}